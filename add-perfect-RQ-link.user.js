// ==UserScript==
// @name           RED: Add a request link ("RQ") to CD rips missing 100% log or cue
// @description    https://redacted.ch/forums.php?action=viewthread&threadid=35409&postid=671981#post671981
// @author         _
// @version        0.1
// @match          https://redacted.ch/torrents.php?id*
// @match          https://redacted.ch/requests.php*perfect_torrentid*
// @run-at         document-end
// @namespace      _
// ==/UserScript==

(() => {
  'use strict';

  const nodes = document.querySelectorAll(".edition_info");
  const groupid = new URL(window.location).searchParams.get("id");
  const torrentid = new URL(window.location).searchParams.get("perfect_torrentid");

  const hitAPI = async (torrentid) => {
    try {
      const res = await fetch(`${location.origin}/ajax.php?action=torrent&id=${torrentid}`);
      return res.json();
    } catch (error) {
      console.log(error);
    }
  };

  const decodeJSON = payload => {
    var txt = document.createElement("textarea");
    txt.innerHTML = payload;
    return txt.value;
  };

  if (groupid) {
    nodes.forEach(node => {
      if (!node.innerText.split("/").pop().trim().includes("CD")) return;
      let target_tr = node.closest('tr').nextElementSibling
      let target_text = target_tr.firstElementChild.firstElementChild.nextElementSibling.innerText
      if (!target_text.includes("Lossless")) return;
      if (target_text.includes("Cue") && target_text.includes("100%") && !target_text.includes("Trumpable")) return;
      let target_span = node.parentNode.nextElementSibling.firstElementChild.firstElementChild;
      if (target_span.innerText.includes("RQ")) return; // another userscript already added RQ, so let's bail
      let torrentid = target_span.innerHTML.match(/id=([0-9]+)/)[1];
      target_span.insertAdjacentHTML("beforeend", ` | <a class="tooltip button_pl" href="requests.php?action=new&groupid=${groupid}&perfect_torrentid=${torrentid}">RQ</a>`);
    });
  } else if (torrentid) {
    hitAPI(torrentid).then(({response, status}) => {
      if (status !== "success") {
        console.log("API request failed; aborting.");
        return;
      }
      const { torrent, group } = response;
      document.getElementById('format_1').checked = true;
      document.getElementById('bitrate_8').checked = true;
      document.getElementById('media_0').checked = true;
      document.getElementById("logcue_tr").removeAttribute("class");
      document.getElementById("needlog").checked = true;
      document.getElementById("minlogscore_span").removeAttribute("class");
      document.getElementById("minlogscore").value = 100;
      document.getElementById("needcue").checked = true;
      document.getElementById("needchecksum").checked = true;
      document.getElementsByName("recordlabel")[0].value = decodeJSON(torrent.remasterRecordLabel) || decodeJSON(group.recordLabel);
      document.getElementsByName("cataloguenumber")[0].value = decodeJSON(torrent.remasterCatalogueNumber) || decodeJSON(group.catalogueNumber);
      document.getElementsByName("year")[0].value = torrent.remasterYear || group.year;
      document.getElementById("description").value = "100% log + cue CD rip with good checksums, please!"
    });
  } else {
    console.log("Oops, something went wrong.");
  }
})();